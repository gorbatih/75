import java.util.ArrayList;
import java.util.List;

/**
 * @author slavca
 * @since 31.08.14
 */
public class Channel
{
    private int id;
    private List<Tick> tickList;
    private long maxTime;
//----------------------------------------------------------------------

    public Channel(int id)
    {
        this.id = id;

        tickList = new ArrayList<Tick>();
    }
//----------------------------------------------------------------------

    public int getId()
    {
        return id;
    }

    public List<Tick> getTickList()
    {
        return tickList;
    }

    public long getMaxTime()
    {
        return maxTime;
    }

    public void setMaxTime(long maxTime)
    {
        this.maxTime = maxTime;
    }
//----------------------------------------------------------------------

    public void addTick(Tick tick)
    {
        tickList.add(tick);
    }
}
