import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author slavca
 * @since 31.08.14
 */
public class Main2
{
    private static final long PERIOD = 10;//10ms
    private static final int CHANNEL_COUNT = 27;

    private String scriptName;
//----------------------------------------------------------------------

    public Main2(String scriptName)
    {
        this.scriptName = scriptName;
    }
//----------------------------------------------------------------------

    public void run() throws Exception
    {
        readFile();
    }
//----------------------------------------------------------------------

    private void readFile() throws Exception
    {
        BufferedReader reader = new BufferedReader(new FileReader("data/" + scriptName + ".csv"));

        List<Channel> channelList = new ArrayList<Channel>();
        for (int i = 0; reader.ready();)
        {
            String line = reader.readLine();

            Channel channel = parseChannel(i, line);
            if (channel != null)
            {
                channelList.add(channel);
                i++;
            }
        }
        reader.close();

        long lastTime = channelList.get(0).getMaxTime();
        for (int i = 1; i < channelList.size(); i++)
        {
            if (channelList.get(i).getMaxTime() != lastTime)
            {
                throw new RuntimeException("lastTime not equals for channelId=0 and channelId=" + i);
            }
        }

        convertTime(channelList);

        List<Tick> tickList = merge(channelList);

        validate(tickList);

        BufferedWriter writer = new BufferedWriter(new FileWriter("data/" + scriptName + ".array"));
        writer.write("const char " + scriptName + "Script[] = {");
        for (int i = 0; i < tickList.size(); i++)
        {
            Tick tick = tickList.get(i);

            if (i > 0)
            {
                writer.write(", ");
            }
            if (i % 4 == 0)
            {
                writer.write("\n    ");
            }
            writer.write("0x");
            writer.write(toHexString(tick.getRelativeTime()));
            writer.write(", 0x");
            writer.write(toHexString(tick.getValue()));
        }
        writer.write("};\nconst int " + scriptName + "ScriptLength = " + (tickList.size() * 2));

        writer.write(";\n");
        writer.flush();
        writer.close();
    }

    private Channel parseChannel(int id, String line)
    {
        String[] temp = line.split(";");
        if (temp.length == 0)
        {
            return null;
        }

        Channel channel = new Channel(id);

        long lastTime = 0;
        for (int i = 1; i < temp.length; i += 2)
        {
            if (temp[i].length() == 0)
            {
                continue;
            }

            long time = Long.parseLong(temp[i]);

            if (lastTime > time)
            {
                throw new RuntimeException("time is greater then previous. channelId=" + id + ", prevTime=" + lastTime + ", time=" + time);
            }

            String s = temp[i + 1];
            int value = parseColor(s);

            channel.addTick(new Tick(id, time, value));

            lastTime = time;
        }

        channel.setMaxTime(lastTime);

        return channel;
    }

    private int parseColor(String s)
    {
        int value = Integer.decode(s);
        if (value >= 256)
        {
            throw new RuntimeException("Invalid value: " + value);
        }
        return value;
    }

    private void convertTime(List<Channel> channelList)
    {
        for (Channel channel : channelList)
        {
            convertTickTime(channel.getTickList());
        }
    }

    private void convertTickTime(List<Tick> tickList)
    {
        long lastTime = 0;
        for (Tick tick : tickList)
        {
            tick.setPrevTime(lastTime);
            long diffTime = tick.getTime() - lastTime;
            tick.setRelativeTime((int)(diffTime / PERIOD));
            lastTime = tick.getTime();
        }

        Tick lastTick = new Tick(0, 0, 0);
        int maxRelativePeriod = 255;
        for (int i = 0; i < tickList.size(); i++)
        {
            Tick tick = tickList.get(i);
            if (tick.getRelativeTime() < 0)
            {
                throw new RuntimeException("Negative relative time");
            }

            lastTime = lastTick.getTime();
            while (tick.getRelativeTime() > maxRelativePeriod)
            {
                lastTime += (maxRelativePeriod * PERIOD);

                int value = lastTick.getValue() +
                    ((int)((tick.getValue() - lastTick.getValue()) * (lastTime - lastTick.getTime()) / (tick.getTime() - lastTick.getTime())));
                Tick addTick = new Tick(tick.getId(), lastTime, value);
                addTick.setPrevTime(tick.getPrevTime());
                addTick.setRelativeTime(maxRelativePeriod);

                tickList.add(i, addTick);
                i++;

                tick.setPrevTime(addTick.getTime());
                tick.setRelativeTime(tick.getRelativeTime() - maxRelativePeriod);
            }
            lastTick = tick;
        }
    }

    private List<Tick> merge(List<Channel> channelList)
    {
        List<Tick> tickList = new ArrayList<Tick>();
        for (Channel channel : channelList)
        {
            tickList.addAll(channel.getTickList());
        }
        Collections.sort(tickList, new Comparator<Tick>()
        {
            public int compare(Tick o1, Tick o2)
            {
                long result = o1.getPrevTime() - o2.getPrevTime();
                if (result == 0)
                {
                    result = o1.getId() - o2.getId();
                }
                if (result == 0)
                {
                    throw new RuntimeException("Several channels have time=" + o1.getTime() + ", id=" + o1.getId());
                }
                return result > 0 ? 1 : -1;
            }
        });

        return tickList;
    }

    private void validate(List<Tick> tickList)
    {
        List<Test> testList = new ArrayList<Test>(CHANNEL_COUNT);

        int i = 0;
        for (; i < CHANNEL_COUNT; i++)
        {
            Tick tick = tickList.get(i);
            Test test = new Test(tick);
            test.counter = tick.getRelativeTime();
            testList.add(test);
        }

        boolean sw = false;
        ml:
        while (true)
        {
            for (Test test : testList)
            {
                test.counter--;
                if (test.counter == 0)
                {
                    if (i == tickList.size())
                    {
                        if (sw)
                        {
                            break ml;
                        }
                        i = 0;
                        sw = true;
                    }
                    Tick tick = tickList.get(i);
                    i++;

                    if (test.tick.getId() != tick.getId())
                    {
                        throw new RuntimeException("Invalid sequence in array for tick=" + test.tick + ". Next tick=" + tick);
                    }
                    test.tick = tick;
                    test.counter = tick.getRelativeTime();
                }
            }
        }
    }

    private String toHexString(int value)
    {
        String s = Integer.toHexString(value).toUpperCase();
        if (s.length() > 2)
        {
            throw new RuntimeException("Value is out of range value=" + value);
        }
        if (s.length() < 2)
        {
            return "0" + s;
        }
        return s;
    }
//----------------------------------------------------------------------

    public static void main(String[] args) throws Exception
    {
        new Main2("main").run();
        new Main2("special").run();
    }
//----------------------------------------------------------------------

    private class Test
    {
        private Tick tick;
        private int counter;

        private Test(Tick tick)
        {
            this.tick = tick;
        }
    }
}
