/**
 */
public class Main
{
    public static final int CHANNEL_COUNT = 1;

    public static final int INTERVAL_COUNT = 600;

    public static final int ITERATION_PERIOD = 100;

    public static final int[] CHANNEL = new int[]{10, 255, 2, 100};

    public static final int CD_Y0_INDEX = 0;

    public static final int CD_X1_INDEX = 1;

    public static final int CD_Y1_INDEX = 2;

    public static final int CD_K_INDEX = 3;

    public static final int CD_I_INDEX = 4;

    public static void main(String[] args)
    {
        int i = 0;
        int[][] channelData = new int[CHANNEL_COUNT][5];
        while (i < INTERVAL_COUNT)
        {
            for (int j = 0; j < ITERATION_PERIOD; j++)
            {
                for (int c = 0; c < CHANNEL_COUNT; c++)
                {
                    int k = channelData[c][CD_K_INDEX];
                    int x2 = channelData[c][CD_X1_INDEX];
                    if (k == x2)
                    {
                        int index = channelData[c][CD_I_INDEX];
                        int[] data = getValue(c, index);
                        channelData[c][CD_I_INDEX] = index + 2;
                        channelData[c][CD_X1_INDEX] = data[0] * ITERATION_PERIOD;
                        channelData[c][CD_Y0_INDEX] = channelData[c][CD_Y1_INDEX];
                        channelData[c][CD_Y1_INDEX] = data[1];
                        channelData[c][CD_K_INDEX] = 0;
                        k = 0;
                    }
                    int y0 = channelData[c][CD_Y0_INDEX];
                    int y1 = channelData[c][CD_Y1_INDEX];
                    int x1 = channelData[c][CD_X1_INDEX];
                    int value = (int) ((float) (k * (y1 - y0)) / (float) x1) + y0;
                    channelData[c][CD_K_INDEX] = k + 1;

                    System.out.println(i + ", " + j + ", " + value);
                }
            }
            i++;
        }
    }

    public static int[] getValue(int channel, int index)
    {
        if (index >= CHANNEL.length)
        {
            return new int[]{CHANNEL[CHANNEL.length - 2], CHANNEL[CHANNEL.length - 1]};
        }
        else
        {
            return new int[]{CHANNEL[index], CHANNEL[index + 1]};
        }
    }
}

/*
1, 2, 3, 4,
 */

