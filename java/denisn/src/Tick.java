/**
 * @author slavca
 * @since 31.08.14
 */
public class Tick
{
    private int id;
    private long time;
    private long prevTime;
    private int relativeTime;
    private int value;
//----------------------------------------------------------------------

    public Tick(int id, long time, int value)
    {
        this.id = id;
        this.time = time;
        this.value = value;
    }
//----------------------------------------------------------------------

    public int getId()
    {
        return id;
    }

    public long getTime()
    {
        return time;
    }

    public long getPrevTime()
    {
        return prevTime;
    }

    public void setPrevTime(long prevTime)
    {
        this.prevTime = prevTime;
    }

    public int getRelativeTime()
    {
        return relativeTime;
    }

    public void setRelativeTime(int relativeTime)
    {
        this.relativeTime = relativeTime;
    }

    public int getValue()
    {
        return value;
    }

    public String toString()
    {
        return "Tick{" +
            "id=" + id +
            ", time=" + time +
            ", prevTime=" + prevTime +
            ", relativeTime=" + relativeTime +
            ", value=" + value +
            '}';
    }
}
