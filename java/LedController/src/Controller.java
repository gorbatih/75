import main.channels.FlowerChannels;

/**
 * User: Master
 * Date: 20.07.14
 * Time: 2:47
 */
public class Controller implements Runnable{
    public static int FLOWERS_COUNT = 7;
    //100 миллисекунд будут означать 1 шаг исполнения программы
    public static int THREAD_SLEEP_TIME_MS = 100;
    FlowerChannels flowers;
    public Controller(){
        init();
    }

    private void init() {
        flowers = new FlowerChannels(FLOWERS_COUNT);
    }


    @Override
    public void run() {
        while (true) {
            flowers.doProgramStep();
            sleep(THREAD_SLEEP_TIME_MS);
        }
    }

    private void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

    }
}
