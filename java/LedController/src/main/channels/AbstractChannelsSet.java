package main.channels;

import main.channel.LedChannel;
import main.program.AbstractProgram;
import main.program.FlowerProgram;
import main.program.LedPlan;

import java.util.ArrayList;

/**
 * Содержит общие для всех наборов каналов(Цветы, подстветка, бутоны) методы
 * User: Master
 * Date: 20.07.14
 * Time: 3:04
 */
public abstract class AbstractChannelsSet {
    private AbstractProgram currentProgram;
    private ArrayList<LedChannel> red;
    private ArrayList<LedChannel> green;
    private ArrayList<LedChannel> blue;
    private int count;

    public AbstractChannelsSet(int count) {
        red = new ArrayList<LedChannel>(count);
        green = new ArrayList<LedChannel>(count);
        red = new ArrayList<LedChannel>(count);
        this.count = count;
    }

    public ArrayList getRed() {
        return red;
    }

    public void setRed(ArrayList red) {
        this.red = red;
    }

    public ArrayList getGreen() {
        return green;
    }

    public void setGreen(ArrayList green) {
        this.green = green;
    }

    public ArrayList getBlue() {
        return blue;
    }

    public void setBlue(ArrayList blue) {
        this.blue = blue;
    }

    // Отвечает на вопрос: Выполняется ли сейчас какая-либо программа на каналах?
    public boolean isInProcess() {
        boolean inProcess = false;
        for(int i = 0; i < count; i++) {
            inProcess = inProcess | red.get(i).isInProcess() | green.get(i).isInProcess() | blue.get(i).isInProcess();
        }
        return inProcess;
    }

    //Выполняет один шаг программы
    public void doProgramStep() {
        if (currentProgram == null || !currentProgram.hasParts()) {
            currentProgram = FlowerProgram.getWhiteWaveProgram();       //тут можно замутить выбор разных программ
        }
        if (!isInProcess()) {
            for(LedPlan plan : currentProgram.getRedBrightnessPlan()) {
                red.get(plan.getLed()).to(plan.getBrightness(), currentProgram.getSteps());
            }
            for(LedPlan plan : currentProgram.getGreenBrightnessPlan()) {
                green.get(plan.getLed()).to(plan.getBrightness(), currentProgram.getSteps());
            }
            for(LedPlan plan : currentProgram.getBlueBrightnessPlan()) {
                blue.get(plan.getLed()).to(plan.getBrightness(), currentProgram.getSteps());
            }
            currentProgram.nextPart();
        }
        for(int i = 0; i < count; i++) {
            red.get(i).step();
            green.get(i).step();
            blue.get(i).step();
        }
    }

    public AbstractProgram getCurrentProgram() {
        return currentProgram;
    }

    public void setCurrentProgram(AbstractProgram currentProgram) {
        this.currentProgram = currentProgram;
    }
}
