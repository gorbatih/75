package main.program;

/**
 * Created with IntelliJ IDEA.
 * User: Master
 * Date: 21.07.14
 * Time: 21:11
 * To change this template use File | Settings | File Templates.
 */
//указывает, какой канал в какую яркость установить
public class LedPlan {
    private int brightness;
    private int led;

    public LedPlan(int brightness, int led) {
        this.brightness = brightness;
        this.led = led;
    }

    public int getBrightness() {
        return brightness;
    }

    public int getLed() {
        return led;
    }
}
