package main.program;

/**
 * Created with IntelliJ IDEA.
 * User: Master
 * Date: 20.07.14
 * Time: 3:06
 * To change this template use File | Settings | File Templates.
 */
public class FlowerProgram extends AbstractProgram {

    private static int[][] emptyProgam = {{min, min, min, min, min, min, min}};
    protected FlowerProgram(LedPlan[][] redPlanningBrightness,
                            LedPlan[][] greenPlanningBrightness,
                            LedPlan[][] bluePlanningBrightness,
                            int steps,
                            int maxParts) {
        super(redPlanningBrightness, greenPlanningBrightness, bluePlanningBrightness, steps, maxParts);
    }

    //<editor-fold desc="Новое описание. Программа 'Белая волна'">
    public static AbstractProgram getWhiteWaveProgram() {
        return new FlowerProgram(whiteWaveRed, whiteWaveGreen, whiteWaveBlue, whiteWaveSteps, whiteWaveParts);
    }
    private static int whiteWaveSteps = 10;
    private static int whiteWaveParts = 11;//количество частей программы
    private static LedPlan[][] whiteWaveRed = {
            {lp(0, low), lp(1, min), lp(2, min), lp(3, min), lp(4, min), lp(5, min), lp(6, min)},
            {lp(0, mid), lp(1, low)},
            {lp(1, mid), lp(2, low)},
            {lp(0, low), lp(2, mid), lp(3, low)},
            {lp(0, min), lp(1, low), lp(3, mid), lp(4, low)},
            {lp(0, low), lp(1, min), lp(2, low), lp(4, mid), lp(5, low)},
            {lp(0, mid), lp(1, low), lp(2, min), lp(3, low), lp(5, mid), lp(6, low)},
            {lp(0, low), lp(1, mid), lp(2, low), lp(3, min), lp(4, low), lp(6, mid)},
            {lp(0, min), lp(1, low), lp(2, mid), lp(3, low), lp(4, min), lp(5, low)},
            {lp(1, min), lp(2, min), lp(4, low), lp(5, min), lp(6, low)},
            {lp(3, min), lp(4, min), lp(6, min)}};
    private static LedPlan[][] whiteWaveGreen = {
            {lp(0, low), lp(1, min), lp(2, min), lp(3, min), lp(4, min), lp(5, min), lp(6, min)},
            {lp(0, mid), lp(1, low)},
            {lp(1, mid), lp(2, low)},
            {lp(0, low), lp(2, mid), lp(3, low)},
            {lp(0, min), lp(1, low), lp(3, mid), lp(4, low)},
            {lp(0, low), lp(1, min), lp(2, low), lp(4, mid), lp(5, low)},
            {lp(0, mid), lp(1, low), lp(2, min), lp(3, low), lp(5, mid), lp(6, low)},
            {lp(0, low), lp(1, mid), lp(2, low), lp(3, min), lp(4, low), lp(6, mid)},
            {lp(0, min), lp(1, low), lp(2, mid), lp(3, low), lp(4, min), lp(5, low)},
            {lp(1, min), lp(2, min), lp(4, low), lp(5, min), lp(6, low)},
            {lp(3, min), lp(4, min), lp(6, min)}};
    private static LedPlan[][] whiteWaveBlue = {
            {lp(0, low), lp(1, min), lp(2, min), lp(3, min), lp(4, min), lp(5, min), lp(6, min)},
            {lp(0, mid), lp(1, low)},
            {lp(1, mid), lp(2, low)},
            {lp(0, low), lp(2, mid), lp(3, low)},
            {lp(0, min), lp(1, low), lp(3, mid), lp(4, low)},
            {lp(0, low), lp(1, min), lp(2, low), lp(4, mid), lp(5, low)},
            {lp(0, mid), lp(1, low), lp(2, min), lp(3, low), lp(5, mid), lp(6, low)},
            {lp(0, low), lp(1, mid), lp(2, low), lp(3, min), lp(4, low), lp(6, mid)},
            {lp(0, min), lp(1, low), lp(2, mid), lp(3, low), lp(4, min), lp(5, low)},
            {lp(1, min), lp(2, min), lp(4, low), lp(5, min), lp(6, low)},
            {lp(3, min), lp(4, min), lp(6, min)}};
    //</editor-fold>

    //<editor-fold desc="Старое описание. Программа 'Бегущие огоньки'">
    private static int sparksFlySteps = 4;
    private static int[][] sparksFlyRed = {
            //огоньки бегут вперёд, 18 шагов
            {low, min, min, min, min, min, min},
            {min, min, low, min, min, min, min},
            {low, min, min, min, low, min, min},
            {min, min, low, min, min, min, min},
            {low, min, min, min, low, min, low},
            {min, min, low, min, min, min, mid},
            {low, min, min, min, low, min, mid},
            {min, min, low, min, min, min, hi},
            {min, min, min, min, low, min, hi},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            //огоньки бегут назад, 9 шагов
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, max, min},
            {min, min, min, hi, hi, min, min},
            {min, min, mid, mid, mid, min, min},
            {min, low, low, low, min, min, min},
            {low, low, low, min, min, min, min},
            {min, min, min, min, min, min, min}};
    private static int[][] sparksFlyGreen = {
            //огоньки бегут вперёд, 18 шагов
            {min, min, min, min, min, min, min},
            {min, low, min, min, min, min, min},
            {min, min, min, low, min, min, min},
            {min, low, min, min, min, low, min},
            {min, min, min, low, min, min, low},
            {min, low, min, min, min, low, low},
            {min, min, min, low, min, min, mid},
            {min, low, min, min, min, low, mid},
            {min, min, min, low, min, min, hi},
            {min, low, min, min, min, low, hi},
            {min, min, min, low, min, min, hi},
            {min, min, min, min, min, low, hi},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            //огоньки бегут назад, 9 шагов
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, max, min},
            {min, min, min, min, hi, hi, min},
            {min, min, min, hi, hi, min, min},
            {min, min, mid, mid, mid, min, min},
            {min, mid, mid, mid, min, min, min},
            {low, low, low, min, min, min, min},
            {min, min, min, min, min, min, min}};
    private static int[][] sparksFlyBlue = {
            //огоньки бегут вперёд, 18 шагов
            {low, min, min, min, min, min, min},
            {low, min, min, min, min, min, min},
            {low, low, min, min, min, min, min},
            {min, low, min, min, min, min, min},
            {min, low, mid, min, min, min, min},
            {min, min, mid, min, min, min, min},
            {min, min, mid, mid, min, min, min},
            {min, min, min, mid, min, min, min},
            {min, min, min, mid, mid, min, min},
            {min, min, min, min, mid, min, min},
            {min, min, min, min, mid, hi, min},
            {min, min, min, min, min, hi, min},
            {min, min, min, min, min, hi, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, min, max},
            //огоньки бегут назад, 9 шагов
            {min, min, min, min, min, min, max},
            {min, min, min, min, min, max, min},
            {min, min, min, min, max, min, min},
            {min, min, min, hi, min, min, min},
            {min, hi, hi, min, min, min, min},
            {mid, mid, min, min, min, min, min},
            {mid, mid, min, min, min, min, min},
            {low, low, min, min, min, min, min},
            {min, min, min, min, min, min, min}};
    //</editor-fold>
}
