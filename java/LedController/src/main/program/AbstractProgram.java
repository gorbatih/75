package main.program;

import java.util.ArrayList;

/**
 * Программа - это последовательный набор состояний каналов.
 * Информация содержится в матрицах. Первый аргумент матрицы - это номер части программы,
 * второй аргумент - это номер канала. Значение в ячейке массива Brightness - яркость,
 * которую нужно будет достигнуть.
 * User: Master
 * Date: 20.07.14
 * Time: 2:58
 */
public abstract class AbstractProgram {
    protected static int min = 0;
    protected static int low = 63;
    protected static int mid = 127;
    protected static int hi = 191;
    protected static int max = 255;
    private int currentPart;
    private int maxParts;
    //Шаги для LedChannels, которые используются в программе
    private int steps;
    private LedPlan[][] redPlanningBrightness;
    private LedPlan[][] greenPlanningBrightness;
    private LedPlan[][] bluePlanningBrightness;

    protected AbstractProgram(
            LedPlan[][] redPlanningBrightness,
            LedPlan[][] greenPlanningBrightness,
            LedPlan[][] bluePlanningBrightness,
            int steps,
            int maxParts) {
        this.redPlanningBrightness = redPlanningBrightness;
        this.greenPlanningBrightness = greenPlanningBrightness;
        this.bluePlanningBrightness = bluePlanningBrightness;
        this.currentPart = 0;
        this.maxParts = maxParts;
    }

    public int getCurrentPart() {
        return currentPart;
    }

    public void nextPart() {
        currentPart++;
    }

    public void setCurrentPart(int currentPart) {
        this.currentPart = currentPart;
    }

    public int getMaxParts() {
        return maxParts;
    }

    public void setMaxParts(int maxParts) {
        this.maxParts = maxParts;
    }

    public boolean hasParts() {
        return currentPart < maxParts;
    }

    public LedPlan[] getRedBrightnessPlan() {
        return redPlanningBrightness[currentPart];
    }

    public LedPlan[] getGreenBrightnessPlan() {
        return greenPlanningBrightness[currentPart];
    }

    public LedPlan[] getBlueBrightnessPlan() {
        return bluePlanningBrightness[currentPart];
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public static LedPlan lp(int led, int brightness) {
        return new LedPlan(brightness, led);
    }
}
