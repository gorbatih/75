package main.channel;

/**
 * Соответствует одной ножке микроконтроллера
 * User: Master
 * Date: 20.07.14
 * Time: 2:16
 */
public class LedChannel {
    public static int MAX = 255;

    private int currentBrightness;
    private int planningBrightness;
    private int steps;
    private boolean inProcess;

    public int getCurrentBrightness() {
        return currentBrightness;
    }

    public void setCurrentBrightness(int currentBrightness) {
        this.currentBrightness = currentBrightness;
    }

    public int getPlanningBrightness() {
        return planningBrightness;
    }

    public void setPlanningBrightness(int planningBrightness) {
        if (planningBrightness > MAX) {
            this.planningBrightness = MAX;
        } else {
            this.planningBrightness = planningBrightness;
        }
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public boolean isInProcess() {
        return inProcess;
    }

    public void setInProcess(boolean inProcess) {
        this.inProcess = inProcess;
    }

    // Устанавливает новое значение яркости для канала, которое необходимо достигнуть за steps шагов
    public void to(int planningBrightness, int steps) {
        setPlanningBrightness(planningBrightness);
        this.steps = steps;
        inProcess = true;
    }

    // Собственно, один шаг к желаемой яркости канала
    public void step() {
        if (!inProcess || steps <= 0) {
            return;
        }
        int delta = (planningBrightness - currentBrightness)/steps;
        currentBrightness += delta;
        steps--;
        if (planningBrightness == currentBrightness) {
            inProcess = false;
            steps = 0;
        }
    }
}
