
#include "main.h"
#include "button.h"
#include "mode.h"

//------------------------------------------------------------------------------

#define CHECK_PERIOD_MS         ( int ) 5
#define CLICK_PERIOD_MS         ( int ) 100
#define LONG_CLICK_PERIOD_MS    ( int ) 900
#define ERROR_PERIOD_MS         ( int ) 1000

#define CHECK_PERIOD_TICKS      CHECK_PERIOD_MS / portTICK_PERIOD_MS
#define CLICK_COUNT             CLICK_PERIOD_MS / CHECK_PERIOD_MS
#define LONG_CLICK_COUNT        LONG_CLICK_PERIOD_MS / CHECK_PERIOD_MS
#define ERROR_COUNT             ERROR_PERIOD_MS / CHECK_PERIOD_MS

//------------------------------------------------------------------------------

void enableButtonIRQ( FunctionalState NewState );

/**
* high = 1 - проверяем высокий уровень, 0 - проверяем низкий уровень
*/
int checkButton( int highCount, int lowCount );

//------------------------------------------------------------------------------

void EXTI15_10_IRQHandler( void )
{
    //Запрет прерывания
    enableButtonIRQ( DISABLE );
    //Отправка события о прерывании
    xQueueSendFromISR( queueButton, 0, NULL );
    //Очистка флага
    EXTI->PR |= EXTI_PR_PR14;
}

/**
* Шаг 1 - проверяем срабатывания кнопки
* Шаг 2 - ждем отпускания кнопки или долгого нажатия
* По текущему режиму работы и короткому/длинному нажатию кнопки вычисляем новый
* режим работы и отправляем соответсвующее событие
*/
void vTaskButton( void *pvParameters )
{
    char message;
    TickType_t irqEnableDelay = ERROR_PERIOD_MS / portTICK_PERIOD_MS;
    for( ;; )
    {
        xQueueReceive( queueButton, &( message ), portMAX_DELAY  );

        int result = checkButton( CLICK_COUNT, CLICK_COUNT );
        if( result == 1 )
        {
            result = checkButton( LONG_CLICK_COUNT, CLICK_COUNT );
            if( result == 1 )
            {
                MODE75_switchMode( SPECIAL );
            }
            else if( result == 0 )
            {
                MODE75_switchMode( ON );
            }
            else
            {
                MODE75_switchMode( OFF );
            }
        }
        else if( result == -1 )
        {
            MODE75_switchMode( OFF );
        }

        vTaskDelay( irqEnableDelay );
        enableButtonIRQ( ENABLE );
    }
}

//------------------------------------------------------------------------------

/**
* Разрешить/запретить прерывания для кнопки
*   This parameter can be: ENABLE or DISABLE.
*/
void enableButtonIRQ( FunctionalState NewState )
{
    if (NewState != DISABLE)
    {
        EXTI->IMR |= BUTTON_IRQ;
    }
    else
    {
        EXTI->IMR &= ~BUTTON_IRQ;
    }
}

int checkButton( int highCount, int lowCount )
{
    int lastState = 0;
    int lastStateI = 0;
    for( int i = 0; i < ERROR_COUNT ; i++ )
    {
        if( BUTTON_PORT->IDR & BUTTON_PIN )
        {
            if( !lastState )
            {
                lastState = 1;
                lastStateI = i;
                continue;
            }
        }
        else if( lastState )
        {
            lastState = 0;
            lastStateI = i;
            continue;
        }
        if( ( lastState && ( i - lastStateI ) >= highCount ) ||
            ( !lastState && ( i - lastStateI ) >= lowCount ) )
        {
            return lastState;
        }
        vTaskDelay( CHECK_PERIOD_TICKS );
    }
    return -1;
}
