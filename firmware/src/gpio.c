
#include "main.h"
#include "gpio.h"

//------------------------------------------------------------------------------

#define LED_PINA        ( GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_15 )
#define LED_PINB        ( GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 )
#define LED_PINC        ( GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 )
#define LED_PINC_SW     ( GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 )

#define TIM_PRESCALER   720

//------------------------------------------------------------------------------

const int hwPwmChannelsLength = 24;
__IO uint16_t *hwPwmChannels[] = {
  &(TIM4->CCR4), &(TIM4->CCR3), &(TIM4->CCR2),  //led1
  &(TIM4->CCR1), &(TIM2->CCR2), &(TIM2->CCR1),  //led2
  &(TIM1->CCR4), &(TIM1->CCR3), &(TIM1->CCR2),  //led3
  &(TIM1->CCR1), &(TIM8->CCR4), &(TIM8->CCR3),  //led4
  &(TIM8->CCR2), &(TIM8->CCR1), &(TIM2->CCR4),  //led5
  &(TIM2->CCR3), &(TIM3->CCR4), &(TIM3->CCR3),  //led6
  &(TIM3->CCR2), &(TIM3->CCR1), &(TIM5->CCR4),  //led7
  &(TIM5->CCR3), &(TIM5->CCR2), &(TIM5->CCR1)   //background
};
const int swPwmChannelsLength = 3;
uint16_t swPwmChannels[] = { GPIO_Pin_1, GPIO_Pin_2, GPIO_Pin_3 };
char swPwmValues[] = {0, 0, 0};

char initLed = 2;

//------------------------------------------------------------------------------

void prvInitHwPwmTimer( TIM_TypeDef *TIMx, int frequencyPrescaler );
void prvInitSwPwmTimer( TIM_TypeDef *TIMx );
void prvClockCmdDelay( void );

//------------------------------------------------------------------------------

void GPIO75_init( void )
{
    //включаем тактирование: портов A, B, C, D; альтернативных функций;
    // тактирования I/O для альтернативных функций
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
      RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO | RCC_APB2ENR_AFIOEN,
      ENABLE );

    //задержка для ожидания включения тактирования
    prvClockCmdDelay();

    //remap для таймера TMI2
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
    GPIO_PinRemapConfig(GPIO_FullRemap_TIM2, ENABLE);

    //деинициализация портов
    GPIO_DeInit( GPIOA );
    GPIO_DeInit( GPIOB );
    GPIO_DeInit( GPIOC );

    //выключение внешнего низкоскоростного осцилятора
    RCC_LSEConfig(RCC_LSE_OFF);


    GPIO_InitTypeDef gpio;

    //----начало инициализации прерывания на PC14-------------------------------
    //нога Button настроена на вход с поддяжкой к земле
    gpio.GPIO_Pin = BUTTON_PIN;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_Init(BUTTON_PORT, &gpio);

    //говорим что надо порождать прерывания на PC14 (PC - 2, 14 - AFIO_EXTICR4[11:8])
    AFIO->EXTICR[3] |= AFIO_EXTICR4_EXTI14_PC;

    //генерить прерывание по возрастанию фронта на 14 ноге
    EXTI->RTSR |= EXTI_RTSR_TR14;

    //разрешаем прерывание на 14 ноге
    EXTI->IMR |= BUTTON_IRQ;

    //Разрешаем прерывание
    NVIC_EnableIRQ(EXTI15_10_IRQn);
    //----конец инициализации прерывания на PC14--------------------------------

    //----начало инициализации неиспользуемых ног-------------------------------
    //как аналоговый вход (для уменьшения энергопотребления и защиты от статики)
    gpio.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_12;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOB, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_10 |
      GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_15;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOC, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOD, &gpio);
    //----конец инициализации неиспользуемых ног--------------------------------
}

void GPIO75_resetLED( void )
{
    if( initLed == 0 )
    {
        return;
    }
    initLed = 0;

    //запрещаем прерывания от таймера для эмуляции ШИМ
    NVIC_DisableIRQ( TIM7_IRQn );

    //выключение тактового сигнала на таймера
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, DISABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, DISABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, DISABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, DISABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, DISABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, DISABLE);

    //задержка останова тактирования таймеров
    prvClockCmdDelay();

    GPIO_InitTypeDef gpio;

    //ноги LED настроены на аналоговый вход
    gpio.GPIO_Pin = LED_PINA;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = LED_PINB;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOB, &gpio);

    gpio.GPIO_Pin = LED_PINC | LED_PINC_SW;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init( GPIOC, &gpio);

    //сбрасываем предыдущие значения регистров
    GPIOA->BRR = LED_PINA;

    GPIOB->BRR = LED_PINB;

    GPIOC->BRR = LED_PINC | LED_PINC_SW;
}

void GPIO75_initLED( void )
{
    if( initLed == 1 )
    {
        return;
    }
    initLed = 1;

    //включение тактового сигнала на таймера
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

    //задержка запуска тактирования таймеров
    prvClockCmdDelay();

    //инициализация таймеров
    prvInitHwPwmTimer(TIM1, 2);
    prvInitHwPwmTimer(TIM2, 1);
    prvInitHwPwmTimer(TIM3, 1);
    prvInitHwPwmTimer(TIM4, 1);
    prvInitHwPwmTimer(TIM5, 1);
    prvInitSwPwmTimer(TIM7);
    prvInitHwPwmTimer(TIM8, 2);


    GPIO_InitTypeDef gpio;

    //ноги LED настроены на выход
    gpio.GPIO_Pin = GPIO_Pin_0;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_1;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_2;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_3;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_6;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_7;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_8;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_9;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_10;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_11;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);

    gpio.GPIO_Pin = GPIO_Pin_15;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &gpio);


    gpio.GPIO_Pin = LED_PINB;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOB, &gpio);

    gpio.GPIO_Pin = LED_PINC;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOC, &gpio);

    gpio.GPIO_Pin = LED_PINC_SW;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init( GPIOC, &gpio);

    //разрешаем прерывания от таймера для эмуляции ШИМ
    NVIC_EnableIRQ( TIM7_IRQn );
}

void GPIO75_setLedValue( int index, char value )
{
    if( index < hwPwmChannelsLength )
    {
        if( index == 0 )//gf
        {
            TIM4->CCR4 = value;
        }
        else if( index == 1 )
        {
            TIM4->CCR3 = value;
        }
        else if( index == 2 )
        {
            TIM4->CCR2 = value;
        }
        else if( index == 3 )//gm
        {
            TIM3->CCR2 = value;
        }
        else if( index == 4 )
        {
            TIM3->CCR1 = value;
        }
        else if( index == 5 )
        {
            TIM5->CCR4 = value;
        }
        else if( index == 6 )//fm
        {
            TIM4->CCR1 = value;
        }
        else if( index == 7 )
        {
            TIM2->CCR2 = value;
        }
        else if( index == 8 )
        {
            TIM2->CCR1 = value;
        }
        else if( index == 9 )//u
        {
            TIM2->CCR3 = value;
        }
        else if( index == 10 )
        {
            TIM3->CCR4 = value;
        }
        else if( index == 11 )
        {
            TIM3->CCR3 = value;
        }
        else if( index == 12 )//s
        {
            TIM1->CCR4 = value;
        }
        else if( index == 13 )
        {
            TIM1->CCR3 = value;
        }
        else if( index == 14 )
        {
            TIM1->CCR2 = value;
        }
        else if( index == 15 )//d
        {
            TIM8->CCR2 = value;
        }
        else if( index == 16 )
        {
            TIM8->CCR1 = value;
        }
        else if( index == 17 )
        {
            TIM2->CCR4 = value;
        }
        else if( index == 18 )//m
        {
            TIM1->CCR1 = value;
        }
        else if( index == 19 )
        {
            TIM8->CCR4 = value;
        }
        else if( index == 20 )
        {
            TIM8->CCR3 = value;
        }
        else if( index == 21 )//b
        {
            TIM5->CCR3 = value;
        }
        else if( index == 22 )
        {
            TIM5->CCR2 = value;
        }
        else if( index == 23 )
        {
            TIM5->CCR1 = value;
        }
//        *hwPwmChannels[ index ] = value;
    }
    else
    {
        swPwmValues[ index - hwPwmChannelsLength ] = value;
    }
}

void GPIO75_setSwPwm( int index, char value )
{
    if( value == 1 )
    {
        GPIOC->BSRR |= swPwmChannels[ index ];
    }
    else
    {
        GPIOC->BRR |= swPwmChannels[ index ];
    }
}

char GPIO75_getSwPwm( int index )
{
    return GPIOC->IDR & swPwmChannels[ index ];
}

//------------------------------------------------------------------------------

void prvInitHwPwmTimer( TIM_TypeDef *TIMx, int frequencyPrescaler )
{
    // Конфигурация таймера
    TIM_TimeBaseInitTypeDef timer;
    TIM_TimeBaseStructInit(&timer);                                     //Заполняем поля структуры дефолтными значениями
    timer.TIM_Prescaler = (uint16_t) (MAIN_CLOCK_HZ / ( 25600 * frequencyPrescaler )) - 1;       // Запускаем таймер на тактовой частоте в 25600 Hz
    timer.TIM_Period = 255;                                             // Период - 256 тактов => 25600/256 = 100 Hz
    timer.TIM_ClockDivision = 0;
    timer.TIM_CounterMode = TIM_CounterMode_Up;                         // Отсчет от нуля до TIM_Period
    TIM_TimeBaseInit(TIMx, &timer);

    // Конфигурация выхода таймера
    TIM_OCInitTypeDef timerOut;
    timerOut.TIM_OCMode = TIM_OCMode_PWM1;              // Конфигурируем выход таймера, режим - PWM1
    timerOut.TIM_OutputState = TIM_OutputState_Enable;  // Собственно - выход включен
    timerOut.TIM_Pulse = 0;                             // Пульс длинной 0 тактов => 0/256 = 0%
    timerOut.TIM_OCPolarity = TIM_OCPolarity_High;      // Полярность => пульс - это единица (+3.3V)

    if( frequencyPrescaler == 2 )
    {
        timerOut.TIM_OutputNState = TIM_OutputNState_Disable;
    }

    // Инициализируем все выходы таймера
    TIM_OC1Init(TIMx, &timerOut);
    TIM_OC2Init(TIMx, &timerOut);
    TIM_OC3Init(TIMx, &timerOut);
    TIM_OC4Init(TIMx, &timerOut);

    // Разрешаем предварительную загрузку регистра сравнения
    TIM_OC1PreloadConfig(TIMx, TIM_OCPreload_Enable);
    TIM_OC2PreloadConfig(TIMx, TIM_OCPreload_Enable);
    TIM_OC3PreloadConfig(TIMx, TIM_OCPreload_Enable);
    TIM_OC4PreloadConfig(TIMx, TIM_OCPreload_Enable);
    // Разрешаем предварительную загрузку регистра автоперезагрузки
    TIM_ARRPreloadConfig(TIMx, ENABLE);

    if( frequencyPrescaler == 2 )
    {
        /* TIM1/TIM8 Main Output Enable */
        TIM_CtrlPWMOutputs(TIMx, ENABLE);
    }

    // Включаем таймер
    TIM_Cmd(TIMx, ENABLE);
}

void prvInitSwPwmTimer( TIM_TypeDef *TIMx )
{
    // Конфигурация таймера
    TIM_TimeBaseInitTypeDef timer;
    TIM_TimeBaseStructInit(&timer);                                     //Заполняем поля структуры дефолтными значениями
    timer.TIM_Prescaler = (uint16_t) (MAIN_CLOCK_HZ / ( 2560000 )) - 1; // Запускаем таймер на тактовой частоте в 2560 kHz
    timer.TIM_Period = 99;                                              // Период - 100 тактов => 2560000/100 = 25600 Hz
    TIM_TimeBaseInit(TIMx, &timer);

    // Разрешаем прерывания от таймера
    TIMx->DIER |= TIM_DIER_UIE;

    // Разрешаем предварительную загрузку регистра автоперезагрузки
    TIM_ARRPreloadConfig(TIMx, ENABLE);

    // Включаем таймер
    TIM_Cmd(TIMx, ENABLE);
}

void prvClockCmdDelay()
{
    volatile unsigned long i=0;
    i++; i++; i++; i++; i++;
    i=0;
}

