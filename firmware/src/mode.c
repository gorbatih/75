
#include "main.h"
#include "mode.h"
#include "gpio.h"
#include "script.h"

//------------------------------------------------------------------------------

void MODE75_switchMode( LedMode mode )
{
    if( mode == ON )
    {
        if( ledMode == ON || ledMode == SPECIAL )
        {
            mode = OFF;
        }
    }
    else if( mode == SPECIAL )
    {
        if( ledMode == OFF )
        {
            mode = ON;
        }
        else if( ledMode == SPECIAL )
        {
            return;
        }
    }

    //Отправка события о смене режима работы
    xQueueSend( queueMode, &mode, 0 );
}

void MODE75_special()
{
    ledMode = ON;
}

void MODE75_off()
{
    //Выключаем таймеры/порты
    GPIO75_resetLED();
    //Останавливаем задачу
    vTaskSuspend( taskScript );
}

//------------------------------------------------------------------------------

void vTaskMode( void *pvParameters )
{
    for( ;; )
    {
        //Получение нового режима работы и сохранение его
        xQueueReceive( queueMode, &ledMode, portMAX_DELAY  );

        if( ledMode == ON )
        {
            //Запускаем таймеры/порты
            GPIO75_initLED();
            //Включаем в качестве текущего основной скрипт
            SCRIPT75_switchToMain();
            //Запускаем задачу
            vTaskResume( taskScript );
        }
        else if( ledMode == SPECIAL )
        {
            //Запускаем таймеры/порты
            GPIO75_initLED();
            //Включаем в качестве текущего специальный скрипт
            SCRIPT75_switchToSpecial();
            //Запускаем задачу
            vTaskResume( taskScript );
        }
        else
        {
            //Выключаем выполнение скрипта
            SCRIPT75_switchToOff();
        }
    }
}
