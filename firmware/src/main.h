#ifndef __MAIN_H
#define __MAIN_H

//------------------------------------------------------------------------------

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* Library includes. */
#include "stm32f10x_conf.h"

//------------------------------------------------------------------------------

#define MAIN_CLOCK_HZ   configCPU_CLOCK_HZ

#define BUTTON_IRQ      EXTI_IMR_MR14

#define BUTTON_PORT     GPIOC
#define BUTTON_PIN      GPIO_Pin_14

typedef enum {OFF = 0, ON = 1, SPECIAL = 2} LedMode;

//------------------------------------------------------------------------------

/**
* Текущий режим работы
*/
extern LedMode ledMode;

/**
* Очередь для обработки прерывания кнопки
*/
extern QueueHandle_t queueButton;
/**
* Очередь для значений вкл/выкл/смена программы
*/
extern QueueHandle_t queueMode;

//------------------------------------------------------------------------------

#endif /* __MAIN_H */