
/* Standard includes. */
#include <stdio.h>

/* Project includes. */
#include "main.h"
#include "button.h"
#include "gpio.h"
#include "mode.h"
#include "script.h"
#include "sw_pwm.h"

//------------------------------------------------------------------------------

/* Task priorities. */
#define mainLED_PRIORITY        ( tskIDLE_PRIORITY + 4 )
#define mainSW_PWM_PRIORITY     ( tskIDLE_PRIORITY + 3 )
#define mainBUTTON_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainMODE_PRIORITY       ( tskIDLE_PRIORITY + 1 )

#define STACK_SIZE              128

//------------------------------------------------------------------------------

LedMode ledMode = OFF;

/* The queue used to send messages to the LCD task. */
QueueHandle_t queueButton;
QueueHandle_t queueMode;

//------------------------------------------------------------------------------

void initClockHSI32MHz( void );
void prvSetupHardware( void );
void vTaskLED( void *pvParameters );

//------------------------------------------------------------------------------

int main( void )
{
    prvSetupHardware();

    //очередь и задача для обработки нажатия кнопки и выбора режима работы
    queueButton = xQueueCreate( 10, sizeof ( char ) );
    xTaskCreate(vTaskButton, "BUTTON", STACK_SIZE, NULL, mainBUTTON_PRIORITY, NULL);

    //очередь и задача для обработки смены режима работы
    queueMode = xQueueCreate( 10, sizeof ( LedMode ) );
    xTaskCreate(vTaskMode, "MODE", STACK_SIZE, NULL, mainMODE_PRIORITY, NULL);

    //задача для выполнения скрипта
    xTaskCreate(vTaskScript, "SCRIPT", STACK_SIZE, NULL, mainLED_PRIORITY, &taskScript );
    vTaskSuspend( taskScript );

    //запуск диспетчера задач ОС
    vTaskStartScheduler();

    //здесь окажемся только при нехватке памяти для создания idle-задачи
    return 0;
}
//------------------------------------------------------------------------------

/**
* Настройка тактового сигнала 32MHz от HSI.
*
* Для FLASH включается буфер предварительной выборки, ставится пропуск в 2
* такта, т.к. частота >48MHz.
*
* От HSI через PLL с коэфициентом умножения 16.
*
* На AHB без предделителя, на AHB1 и AHB2 с предделителем равным 2.
*/
void initClockHSI32MHz()
{
    //Частота SystemCoreClock выше 24 MHz - разрешить буфер предварительной выборки FLASH
    FLASH->ACR|=  FLASH_ACR_PRFTBE;        //Включить буфер предварительной выборки
    FLASH->ACR&= ~FLASH_ACR_LATENCY;       //Очистить FLASH_ACR_LATENCY
    FLASH->ACR |= FLASH_ACR_LATENCY_1;     //Пропускать 1 такт

    //Настройка PLL
    RCC->CFGR  &= ~RCC_CFGR_PLLSRC;        //Источником сигнала для PLL выбран HSI с делением на 2
    RCC->CR   &= ~RCC_CR_PLLON;            //Отключить генератор PLL
    RCC->CFGR &= ~RCC_CFGR_PLLMULL;        //Очистить PLLMULL
    RCC->CFGR |=  RCC_CFGR_PLLMULL8;       //Коефициент умножения = 8
    RCC->CR   |=  RCC_CR_PLLON;            //Включить генератор PLL
    while((RCC->CR & RCC_CR_PLLRDY)==0) {} //Ожидание готовности PLL

    //Переключиться на тактирование от PLL
    RCC->CFGR &= ~RCC_CFGR_SW;             //Очистка битов выбора источника тактового сигнала
    RCC->CFGR |=  RCC_CFGR_SW_PLL;         //Выбрать источником тактового сигнала PLL
    while((RCC->CFGR&RCC_CFGR_SWS)!=0x08){}//Ожидание переключения на PLL

    //Настроить делитель для AHB
    RCC->CFGR &= ~RCC_CFGR_HPRE;           //Очистка битов предделителя "AHB Prescaler"
    RCC->CFGR |=  RCC_CFGR_HPRE_DIV1;      //Установить "AHB Prescaler" равным 1

    //Настроить делитель для шины APB1
    RCC->CFGR &= ~RCC_CFGR_PPRE1;          //Очистка битов предделителя "APB1 Prescaler"
    RCC->CFGR |=  RCC_CFGR_PPRE1_DIV2;     //Установить "APB1 Prescaler" равным 2

    //Настроить делитель для шины APB2
    RCC->CFGR &= ~RCC_CFGR_PPRE2;          //Очистка битов предделителя "APB2 Prescaler"
    RCC->CFGR |=  RCC_CFGR_PPRE2_DIV2;     //Установить "APB2 Prescaler" равным 2
}

void prvSetupHardware()
{
    initClockHSI32MHz();

    /* Set the Vector Table base address at 0x00000000 */
    NVIC_SetVectorTable( NVIC_VectTab_FLASH, 0x0 );
    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );

    /* Configure HCLK clock as SysTick clock source. */
    SysTick_CLKSourceConfig( SysTick_CLKSource_HCLK );

    GPIO75_init();
    GPIO75_resetLED();
}

//------------------------------------------------------------------------------

void vTaskLED( void *pvParameters )
{
    TickType_t delay = 3000 / portTICK_PERIOD_MS;
    for( ;; )
    {
        vTaskDelay(delay);
        GPIO75_setLedValue( 3, 63 );
        GPIO75_setLedValue( 24, 63 );

        vTaskDelay(delay);
        GPIO75_setLedValue( 3, 31 );
        GPIO75_setLedValue( 24, 31 );

        vTaskDelay(delay);
        GPIO75_setLedValue( 3, 15 );
        GPIO75_setLedValue( 24, 15 );

        vTaskDelay(delay);
        GPIO75_setLedValue( 3, 7 );
        GPIO75_setLedValue( 24, 7 );

        vTaskDelay(delay);
        GPIO75_setLedValue( 3, 3 );
        GPIO75_setLedValue( 24, 3 );

        vTaskDelay(delay);
        GPIO75_setLedValue( 3, 0 );
        GPIO75_setLedValue( 24, 0 );
    }
}

#ifdef  DEBUG
/* Keep the linker happy. */
void assert_failed( unsigned char* pcFile, unsigned long ulLine )
{
    for( ;; )
    {
    }
}
#endif
