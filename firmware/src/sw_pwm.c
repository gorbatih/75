
#include "main.h"
#include "gpio.h"
#include "sw_pwm.h"

//------------------------------------------------------------------------------

char swPwmI = 0;

//------------------------------------------------------------------------------

void TIM7_IRQHandler( void )
{
    for( int j=0; j < swPwmChannelsLength; j++ )
    {
        if( swPwmI == 0 )
        {//устанавливаем высокий уровень в начале такта (если требуется)
            if( swPwmValues[j] > 0 )
            {
                GPIO75_setSwPwm( j, 1 );
            }
            else
            {
                GPIO75_setSwPwm( j, 0 );
            }
            continue;
        }
        if( swPwmI > swPwmValues[j] && GPIO75_getSwPwm( j ) )
        {//сбрасываем в низкий уровень в
            GPIO75_setSwPwm( j, 0 );
        }
    }

    if( swPwmI == 255 )
    {
        swPwmI = 0;
    }
    else
    {
        swPwmI++;
    }
    
    //Очистка флага
    TIM7->SR &= ~TIM_SR_UIF;
}
